CALL cd app/
CALL mvn clean install
CALL cd ..
CALL docker-compose down -v
CALL docker build -t pia/mukanova .
CALL docker-compose up
