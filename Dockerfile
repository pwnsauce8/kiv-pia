FROM adoptopenjdk/openjdk15:alpine-jre
EXPOSE 8080
VOLUME /tmp
COPY app/target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]

#https://iamvickyav.medium.com/deploying-spring-boot-war-in-tomcat-based-docker-2b689b206496