# Semestrální práce z předmětu KIV/PIA
https://bitbucket.org/pwnsauce8/kiv-pia/src/master/

## Spuštění aplikaci
V kořenovém adresaři spustit skript `./run.sh` pro Linux nebo `run.bat` pro Windows.

## Administrator
email: `admin@admin.com`
password: `admin`

## Session
Session inactive time: 10m

## Technology
* Java 15
* Spring Boot 2
* Hibernate (JPA)
* PostgreSQL 

## Bonus parts
* password strength evaluation - **2 points**
* password reset using a security question - **2 points**
* password reset using an e-mail (reset link) - **5 points**
* in-game chat - **5 points**
* save games with all turns and allow replay - **5 points**