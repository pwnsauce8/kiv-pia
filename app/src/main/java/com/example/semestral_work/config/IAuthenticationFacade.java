package com.example.semestral_work.config;

import org.springframework.security.core.Authentication;

/**
 * Interface to fully leverage the Spring dependency injection and be able to retrieve the authentication everywhere
 */
public interface IAuthenticationFacade {
    Authentication getAuthentication();
}