package com.example.semestral_work.repos;

import com.example.semestral_work.domain.PlayersInstance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GamePlayersRepo  extends JpaRepository<PlayersInstance, Integer> {
    PlayersInstance findByGameId(int id);
    PlayersInstance findById(Long id);
}
