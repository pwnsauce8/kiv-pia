package com.example.semestral_work.repos;

import com.example.semestral_work.domain.Relationship;
import com.example.semestral_work.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RelationshipRepo extends JpaRepository<Relationship, Long> {

    @Query("SELECT u FROM Relationship u WHERE u.first_user = ?1 and u.second_user = ?2")
    Relationship findAllByFirst_userAndSecond_user(User first_user, User second_user);
}
