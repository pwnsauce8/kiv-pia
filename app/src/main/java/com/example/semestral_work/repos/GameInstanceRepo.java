package com.example.semestral_work.repos;

import com.example.semestral_work.domain.GameInstance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameInstanceRepo extends JpaRepository<GameInstance, Integer> {
    GameInstance findById(int id);
}
