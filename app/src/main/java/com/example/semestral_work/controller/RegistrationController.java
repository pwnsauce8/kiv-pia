package com.example.semestral_work.controller;

import com.example.semestral_work.domain.User;
import com.example.semestral_work.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Map;
import java.util.UUID;

@Controller
public class RegistrationController {

    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public String login(Model model) {
        User user = new User();
        user.setUsername("admin");

        userService.addAdmin(user);
        return "login.html";
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        return "registration.html";
    }

    @RequestMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }

    /**
     * New user registration
     * @param passwordConfirm
     * @param question
     * @param answer
     * @param user
     * @param bindingResult
     * @param model
     * @return template
     */
    @PostMapping("/registration")
    public String addUser(
            @RequestParam("password2") String passwordConfirm,
            @RequestParam("question") String question,
            @RequestParam("answer") String answer,
            @Valid User user,
            BindingResult bindingResult,
            Model model
    ) {
        if (userService.getUserByName(user.getUsername()) != null) {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "User with this name already exists!");
            return "registration.html";
        }

        if (!userService.userExist(user.getEmail())) {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "User with this email already exist!");
            return "registration.html";
        }

        if (StringUtils.isEmpty(passwordConfirm)) {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Password confirmation cannot be empty!");
            return "registration.html";
        }

        if (user.getPassword() != null && !user.getPassword().equals(passwordConfirm)) {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Passwords are different!");
            return "registration.html";
        }

        if (bindingResult.hasErrors()) {
            Map<String, String> errors = ControllerUtils.getErrors(bindingResult);
            model.mergeAttributes(errors);
            return "registration.html";
        }

        if (StringUtils.isEmpty(question)) {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Question cannot be empty!");
            return "registration.html";
        }

        if (StringUtils.isEmpty(answer)) {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Answer cannot be empty!");
            return "registration.html";
        }

        if (!userService.addUser(user)) {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "User already exists!");
            return "registration.html";
        }

        model.addAttribute("messageType", "info");
        model.addAttribute("message", "Thanks for your registration, please check your inbox!");
        return "login.html";
    }

    /**
     * Actiovate user
     * @param model
     * @param code
     * @return template
     */
    @GetMapping("/activate/{code}")
    public String activate(Model model, @PathVariable String code) {
        boolean isActivated = userService.activateUser(code);

        if (isActivated) {
            model.addAttribute("messageType", "info");
            model.addAttribute("message", "User successfully activated");
        } else {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Activation code is not found!");
        }
        return "login.html";
    }

    /**
     * Logout user
     * @param request
     * @param response
     * @return template
     */
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login";
    }

    /**
     * Forgot password template
     */
    @GetMapping("/forgotPassword")
    public String forgotPassword(Map<String, Object> model) {
        return "forgotPassword.html";
    }

    /**
     * Forgot password method
     * @param model
     * @param email
     * @param question
     * @param answer
     * @return template
     */
    @PostMapping("/forgotPassword")
    public String forgotPassword(Model model, @RequestParam("email") String email,
                                 @RequestParam("question") String question,
                                 @RequestParam("answer") String answer) {
        User user = userService.getUserByEmail(email);

        if (user != null) {
            // Check question
            if (!userService.checkQuestion(user, question, answer)) {
                model.addAttribute("messageType", "danger");
                model.addAttribute("message", "Question and answer are not correct!");
                return "forgotPassword.html";
            }

            // Send mail
            user.setActive(false);
            user.setActivationCode(UUID.randomUUID().toString());

            userService.saveUser(user);

            String message = String.format(
                    "To complete the password reset process, please click here: "
                            + "http://localhost:8080/confirmReset/%s",
                                user.getActivationCode()
            );
            userService.sendMessage(user, "Complete Password Reset!", message);

            model.addAttribute("messageType", "info");
            model.addAttribute("message", "Request to reset password received. Check your inbox for the reset link.");
        } else {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "This email address does not exist!");
        }

        return "forgotPassword.html";
    }

    /**
     * Confirm reset password
     * @return template
     */
    @GetMapping("/confirmReset/{code}")
    public String confirmReset(Model model,
                                @PathVariable String code) {
        User user = userService.getActiveUser(code);

        if (user == null) {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Activation code is not found!");
        } else {
            model.addAttribute("user", user);
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Activation code is not found!");
        }

        return "resetPassword.html";
    }

    /**
     * Confirm reset password
     * @return template
     */
    @PostMapping("/resetPassword")
    public String resetPassword(Model model,
                                @RequestParam("email") String email,
                                @RequestParam("password") String password,
                                @RequestParam("password2") String password2,
                                HttpServletRequest request, HttpServletResponse response) {
        User user = userService.getUserByEmail(email);

        if (user == null) {
            return "resetPassword.html";
        }

        if (!password.equals(password2)) {
            model.addAttribute("passwordError", "Passwords are different!");
            return "resetPassword.html";
        }

        if (StringUtils.isEmpty(password)) {
            model.addAttribute("passwordError", "Password cannot be empty");
            return "resetPassword.html";
        }

        if (StringUtils.isEmpty(password2)) {
            model.addAttribute("password2Error", "Password confirmation cannot be empty");
            return "resetPassword.html";
        }
        userService.updatePassword(user, user.getUsername(), password, email);

        model.addAttribute("messageType", "info");
        model.addAttribute("message", "Password successfully reset. You can now log in with the new credentials.");

        // Logout
        userService.destroySession(user);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }

        return "login";
    }
}
