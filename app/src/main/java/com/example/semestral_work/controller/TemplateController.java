package com.example.semestral_work.controller;

import com.example.semestral_work.domain.enums.GameStatus;
import com.example.semestral_work.domain.helpers.ListedGameSimpleForm;
import com.example.semestral_work.service.GamePlayService;
import com.example.semestral_work.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


@Controller
public class TemplateController {

    @Autowired
    private GamePlayService gamePlayCalc;

    @Autowired
    private UserService userSevice;

    @RequestMapping("/main**")
    public Object menu2(HttpServletRequest request){
        String queryString = request.getQueryString();
        if (queryString != null && queryString.startsWith("?")) {
            return "redirect:/main";
        }

        return "main";
    }

    @RequestMapping("/main")
    public Object menu(Model model, HttpServletRequest request, HttpServletResponse response){
        List<ListedGameSimpleForm> newGames = gamePlayCalc.getGameList(GameStatus.WAITING);

        if (!userSevice.isOnline(userSevice.getLoggedUser())) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth != null){
                new SecurityContextLogoutHandler().logout(request, response, auth);
            }
            return "redirect:/login";
        }

        model.addAttribute("onlineUsers", userSevice.getOnlineUsers());
        model.addAttribute("friends", userSevice.getRelationships(userSevice.getLoggedUser()));
        model.addAttribute("newGames", newGames);
        model.addAttribute("user", userSevice.getLoggedUser());

        return "main";
    }

    @RequestMapping("/game/")
    public Object gamePlay(Model model){
        model.addAttribute("friends", userSevice.getRelationships(userSevice.getLoggedUser()));
        model.addAttribute("user", userSevice.getLoggedUser());
        return "game";
    }

    @RequestMapping("/logs")
    public Object logs(Model model){
        List<ListedGameSimpleForm> newGames = gamePlayCalc.getGameList(GameStatus.ENDED);
        model.addAttribute("newGames", newGames);
        model.addAttribute("user", userSevice.getLoggedUser());
        return "logs";
    }

    @RequestMapping(value="/getFriends", method= RequestMethod.GET)
    public String getFriends(Model model) {
        List<ListedGameSimpleForm> newGames = gamePlayCalc.getGameList(GameStatus.WAITING);

        model.addAttribute("onlineUsers", userSevice.getOnlineUsers());
        model.addAttribute("friends", userSevice.getRelationships(userSevice.getLoggedUser()));
        model.addAttribute("newGames", newGames);
        model.addAttribute("user", userSevice.getLoggedUser());



        return "main";
    }

    @RequestMapping(value="/getOnline", method= RequestMethod.GET)
    public String getOnline(Model model) {
        model.addAttribute("onlineUsers", userSevice.getOnlineUsers());

        return "main :: #onlineUsers";
    }

    @RequestMapping(value="/getGameList", method= RequestMethod.GET)
    public String getGameList(Model model) {
        List<ListedGameSimpleForm> newGames = gamePlayCalc.getGameList(GameStatus.WAITING);
        model.addAttribute("gameList", newGames);

        return "main :: #gameList";
    }

}
