package com.example.semestral_work.controller.rest;

import com.example.semestral_work.domain.Relationship;
import com.example.semestral_work.domain.User;
import com.example.semestral_work.domain.enums.RelationshipStatus;
import com.example.semestral_work.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class UserRestApiController {
    @Autowired
    private UserService userSevice;

    @Autowired
    private SimpMessagingTemplate template;
    /**
     * Delete user by id
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping(method = RequestMethod.POST, value = "/api/users/{id}/delete")
    public Object deleteUser(@PathVariable(value = "id") int id) {
        boolean result = userSevice.deleteUser(id);
        if (result) {
            return result;
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    /**
     * Delete friend
     * @param id1 User id who receive request
     * @param id2 User id who send request
     * @return Relationship instance
     */
    @RequestMapping(method = RequestMethod.POST, value = "/api/friends/delete")
    public Object deleteFriend(@RequestParam(value = "user1")int id1,
                               @RequestParam(value = "user2")int id2) {
        User user1 = userSevice.getUserById(id1); // to
        User user2 = userSevice.getUserById(id2); // from (current)

        if (user1 != null) {
            userSevice.unsubscribe(user2, user1);
            userSevice.unsubscribe(user1, user2);

            template.convertAndSend("/topic/deleteFriend/" + id1, "");

            return userSevice.getRelationships(user2);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    /**
     * Add selected friend to list
     * @param id1 User id who receive request
     * @param id2 User id who send request
     * @return Relationship instance
     */
    @RequestMapping(method = RequestMethod.POST, value = "/api/friends/add")
    public Object addFriend(@RequestParam(value = "user1")int id1,
                            @RequestParam(value = "user2")int id2) {
        User user1 = userSevice.getUserById(id1); // to
        User user2 = userSevice.getUserById(id2); // from (current)

        if (user1 != null && user2 != null) {
            Relationship relationship1 = userSevice.subscribe(user1, user2, RelationshipStatus.WAITING);
            Relationship relationship2 = userSevice.subscribe(user2, user1, RelationshipStatus.REQUEST);

            if (relationship1 != null && relationship2 != null) {
                return userSevice.getRelationships(user2);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
            }
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    /**
     * Approve friend request
     * @param id1 User id who receive request
     * @param id2 User id who send request
     * @return Relationship instance
     */
    @RequestMapping(method = RequestMethod.POST, value = "/api/friends/approveRequest")
    public Object approveRequest(@RequestParam(value = "user1")int id1,
                            @RequestParam(value = "user2")int id2) {
        User user1 = userSevice.getUserById(id1);
        User user2 = userSevice.getUserById(id2);

        if (user1 != null && user2 != null) {
            Relationship relationship = userSevice.updateStatus(user1, user2, RelationshipStatus.APPROVED);
            Relationship relationship2 = userSevice.updateStatus(user2, user1, RelationshipStatus.APPROVED);

            if (relationship != null && relationship2 != null) {
                return userSevice.getRelationships(user2);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
            }
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    /**
     *
     * @param currentUser User instance
     * @param id1 User id who receive request
     * @param id2 User id who send request
     * @return Relationship instance
     */
    @RequestMapping(method = RequestMethod.POST, value = "/api/friends/{id}/cancelRequest")
    public Object cancelRequest(@AuthenticationPrincipal User currentUser,
                                 @RequestParam(value = "user1")int id1,
                                 @RequestParam(value = "user2")int id2) {
        User user1 = userSevice.getUserById(id1);
        User user2 = userSevice.getUserById(id2);

        if (user1 != null && user2 != null) {
            userSevice.unsubscribe(user1, user2);
            userSevice.unsubscribe(user2, user1);

            return userSevice.getRelationships(currentUser);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    /**
     * Ask friend to go online
     * @param currentUser User instance who send request
     * @param id User id who receive request
     * @return status
     */
    @RequestMapping(method = RequestMethod.POST, value = "/api/friends/{id}/ask")
    public Object askFriend(@AuthenticationPrincipal User currentUser, @PathVariable(value = "id") int id) {
        User user = userSevice.getUserById(id);

        if (user != null) {
            String message = String.format(
                    "User %s ask you to connect to Tic-tac-toe",
                    currentUser.getUsername()
            );
            userSevice.sendMessage(user, "Activation code", message);
            return ResponseEntity.status(HttpStatus.OK).body("null");
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    /**
     * Reset user password
     * @param id user id
     * @return User instance
     */
    @RequestMapping(method = RequestMethod.POST, value = "/api/user/{id}/reset")
    public Object resetPassword(@PathVariable(value = "id") int id) {
        User user = userSevice.getUserById(id);

        if (user != null) {
            user.setActive(false);
            user.setActivationCode(UUID.randomUUID().toString());

            userSevice.saveUser(user);

            String message = String.format(
                    "To complete the password reset process, please click here: "
                            + "http://localhost:8080/confirmReset/%s",
                    user.getActivationCode()
            );
            userSevice.sendMessage(user, "Complete Password Reset!", message);

            return user;
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    /**
     * Get online users list
     */
    @RequestMapping(method= RequestMethod.GET, value="/api/user/getOnlineUsers")
    public Object getOnlineUsers(){
        if (userSevice.getOnlineUsers() != null){
            return userSevice.getOnlineUsers();
        }
        else return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
    }

    /**
     * Get friend users list
     */
    @RequestMapping(method= RequestMethod.GET, value="/api/user/getFriends")
    public Object getFriends(@RequestParam(value = "user")String user){
        User user1 = userSevice.getUserByName(user);
        if (user1 != null){
            return userSevice.getRelationships(user1);
        }
        else return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
    }
}