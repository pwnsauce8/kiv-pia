package com.example.semestral_work.controller.errors;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

/**
 * Controller for errors code
 */
@Controller
public class MyErrorController implements ErrorController {

    @Override
    public String getErrorPath() {
        return "/error";
    }


    @GetMapping("/error")
    public String handleError(HttpServletRequest request, Model model) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());

            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                // handle HTTP 404 Not Found error
                model.addAttribute("errorCode", "Page Not Found");
                model.addAttribute("errorDesc", "There's no page here!");

            } else if (statusCode == HttpStatus.FORBIDDEN.value()) {
                // handle HTTP 403 Forbidden error
                model.addAttribute("errorCode", "403");
                model.addAttribute("errorDesc", "Unauthorized!");

            } else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                // handle HTTP 500 Internal Server error
                model.addAttribute("errorCode", "500");
                model.addAttribute("errorDesc", "Internal Server error!");
            } else {
                model.addAttribute("errorCode", "...");
                model.addAttribute("errorDesc", "There's some problems!");
            }
        } else {
            model.addAttribute("errorCode", "...");
            model.addAttribute("errorDesc", "There's some problems!");
        }

        return "/error";
    }

}
