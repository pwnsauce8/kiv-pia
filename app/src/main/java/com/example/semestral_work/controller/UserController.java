package com.example.semestral_work.controller;

import com.example.semestral_work.domain.User;
import com.example.semestral_work.domain.enums.Role;
import com.example.semestral_work.service.Response;
import com.example.semestral_work.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * Display user list
     * @return template
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping
    public String userList(Model model) {
        model.addAttribute("currentUser", userService.getLoggedUser().getUsername());
        model.addAttribute("users", userService.findAll());
        model.addAttribute("user", userService.getLoggedUser());

        return "userList.html";
    }

    /**
     * Display user edit form
     * @return template
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("{user}")
    public String userEditForm(@PathVariable User user, Model model) {
        model.addAttribute("user", user);
        model.addAttribute("roles", Role.values());

        return "userEdit.html";
    }

    /**
     * Save user
     * @return template
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @PostMapping
    public String userSave(
            @RequestParam Map<String, String> form,
            @RequestParam("username") String userName
    ) {
        userService.saveUser(userName, form);

        return "redirect:/user";
    }

    /**
     * Display user list
     * @return template
     */
    @GetMapping("players")
    public String getPlayers(Model model) {
        model.addAttribute("users", userService.findAll());

        return "userlist";
    }

    /**
     * Get logged player
     */
    @RequestMapping(value = "/logged", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<User> getLoggedPlayer() {
        return new Response<>(userService.getLoggedUser(), Response.Status.CREATED );
    }
}