package com.example.semestral_work.controller.socket;

import com.example.semestral_work.domain.helpers.ChatMessagePojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class ChatController {

    @Autowired
    private SimpMessagingTemplate template;

    @MessageMapping("/chat.sendMessage")
    public ChatMessagePojo sendMessage(@Payload ChatMessagePojo chatMessagePojo) {
        template.convertAndSend("/topic/chat/" + chatMessagePojo.getGameId(), chatMessagePojo);
        return chatMessagePojo;
    }
}
