package com.example.semestral_work.controller.rest;

import com.example.semestral_work.domain.GameInstance;
import com.example.semestral_work.domain.helpers.ListedGameSimpleForm;
import com.example.semestral_work.domain.enums.GameStatus;
import com.example.semestral_work.service.GamePlayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GameRestApiController {

    @Autowired
    private GamePlayService gamePlayCalc;

    @Autowired
    private SimpMessagingTemplate template;

    /**
     * Create new game
     * @param player User instance
     * @return Game instance
     */
    @PostMapping("/api/tictactoe/game/newgame")
    public Object createNewGame(@RequestParam(value = "player")String player){
        GameInstance result = gamePlayCalc.createNewGame(player);
        if (result != null){
            return result;
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    /**
     * Join to existing game
     * @param id Game id
     * @param player User instance
     * @return Game instance
     */
    @RequestMapping(method= RequestMethod.POST, value="/api/tictactoe/game/{id}/connect")
    public Object joinGame(@PathVariable(value = "id")int id, @RequestParam(value = "player")String player){
        GameInstance result = gamePlayCalc.joinGame(id, player);
        if(result!=null){
            return result;
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    /**
     * Delete selected game
     * @param id  Game id
     * @return true/false
     */
    @RequestMapping(method= RequestMethod.POST, value="/api/tictactoe/game/{id}/delete")
    public Object deleteGame(@PathVariable(value = "id")int id){
        boolean result = gamePlayCalc.deleteGame(id);
        if (result){
            return result;
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    /**
     * Get current state in game
     * @param id Game id
     * @return Game instance
     */
    @RequestMapping(method= RequestMethod.GET, value="/api/tictactoe/game/{id}/currentstate")
    public Object getGamePlayArea(@PathVariable(value = "id") int id){
        GameInstance game = gamePlayCalc.getCurrentState(id);
        if (game != null){
            return gamePlayCalc.getCurrentState(id);
        }
        else return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
    }

    /**
     * Get winner of game
     * @param id Game id
     * @return User instance
     */
    @RequestMapping(method= RequestMethod.GET, value="/api/tictactoe/game/{id}/getwinner")
    public Object getWinner(@PathVariable(value = "id") int id){
        GameInstance game = gamePlayCalc.getCurrentState(id);
        if (game != null){
            return gamePlayCalc.getWinner(id);
        }
        else return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
    }

    /**
     * Make a move
     * @param id Game id
     * @param move id of cell
     * @param player User instance
     * @return status
     */
    @RequestMapping(method= RequestMethod.POST, value="/api/tictactoe/game/{id}/move")
    public ResponseEntity<String> makeAMove(@PathVariable(value = "id") int id, @RequestParam(value = "move")String move, @RequestParam(value = "player")String player){
        GameInstance game = gamePlayCalc.getCurrentState(id);
        if (gamePlayCalc.setMove(id, player, move)){
            template.convertAndSend("/topic/game-progress/" + id, game);
            return ResponseEntity.status(HttpStatus.OK).body("null");
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    /**
     * End game
     * @param id Game id
     * @param player User instance
     * @return Status
     */
    @RequestMapping(method= RequestMethod.POST, value="/api/tictactoe/game/{id}/endgame")
    public ResponseEntity<String> endGame(@PathVariable(value = "id") int id, @RequestParam(value = "player")String player){
        GameInstance game = gamePlayCalc.getCurrentState(id);
        if (gamePlayCalc.endGame(id, player)){
            template.convertAndSend("/topic/game-progress/" + id, game);
            return ResponseEntity.status(HttpStatus.OK).body("null");
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    /**
     * Reset game and play new
     * @param id Game id
     * @return Status
     */
    @RequestMapping(method= RequestMethod.POST, value="/api/tictactoe/game/{id}/reset")
    public Object reset(@PathVariable(value = "id") int id){
        GameInstance game = gamePlayCalc.resetGame(id);
        if (game != null){
            template.convertAndSend("/topic/game-progress/" + id, game);
            return ResponseEntity.status(HttpStatus.OK).body("null");
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    /**
     * Get list of games
     * @return List of the games
     */
    @RequestMapping(method= RequestMethod.GET, value="/api/tictactoe/game/getGames")
    public Object getGames(){
        List<ListedGameSimpleForm> gameInstances = gamePlayCalc.getGameList(GameStatus.WAITING);
        if (gameInstances != null){
            return gameInstances;
        }
        else return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
    }

}