package com.example.semestral_work.domain.enums;

public enum RelationshipStatus {
    APPROVED(0),
    WAITING(1),
    REQUEST(2);

    private int value;
    private RelationshipStatus(int value) {
        this.value=value;
    }
}
