package com.example.semestral_work.domain.helpers;


import com.example.semestral_work.domain.PlayersInstance;
import com.example.semestral_work.domain.User;
import com.example.semestral_work.domain.enums.GameStatus;

public class ListedGameSimpleForm {
    private int id;
    private PlayersInstance playersInstance;
    private GameStatus status;
    private User winner;

    public ListedGameSimpleForm(int id, PlayersInstance playersInstance, GameStatus status, User winner) {
        this.id = id;
        this.playersInstance = playersInstance;
        this.status = status;
        this.winner = winner;
    }

    public ListedGameSimpleForm(int id, PlayersInstance playersInstance, GameStatus status) {
        this.id = id;
        this.playersInstance = playersInstance;
        this.status = status;
    }

    public ListedGameSimpleForm() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PlayersInstance getPlayersInstance() {
        return playersInstance;
    }

    public void setPlayersInstance(PlayersInstance playersInstance) {
        this.playersInstance = playersInstance;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public User getWinner() {
        return winner;
    }

    public void setWinner(User winner) {
        this.winner = winner;
    }
}
