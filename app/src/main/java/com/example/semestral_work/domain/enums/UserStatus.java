package com.example.semestral_work.domain.enums;

public enum UserStatus {
    INGAME,
    INLOBBY;
}
