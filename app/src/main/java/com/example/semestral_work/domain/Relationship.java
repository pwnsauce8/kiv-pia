package com.example.semestral_work.domain;

import com.example.semestral_work.domain.enums.RelationshipStatus;

import javax.persistence.*;

@Entity
@Table(name = "t_relationship")
public class Relationship {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "first_user")
    private User first_user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "second_user")
    private User second_user;

    @CollectionTable(name = "relationship_status", joinColumns = @JoinColumn(name = "relationship_id"))
    @Enumerated(EnumType.STRING)
    private RelationshipStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getFirst_user() {
        return first_user;
    }

    public void setFirst_user(User first_user) {
        this.first_user = first_user;
    }

    public User getSecond_user() {
        return second_user;
    }

    public void setSecond_user(User second_user) {
        this.second_user = second_user;
    }

    public RelationshipStatus getStatus() {
        return status;
    }

    public void setStatus(RelationshipStatus status) {
        this.status = status;
    }
}
