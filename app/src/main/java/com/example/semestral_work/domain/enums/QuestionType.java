package com.example.semestral_work.domain.enums;

public enum QuestionType {
    BOOK("book"),
    ROAD("road"),
    MOTHER("mother"),
    PET("pet"),
    COMPANY("company"),
    SPOUSE("spouse"),
    SCHOOL("school");

    private String value;
    QuestionType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public QuestionType getQuestion(String value) {
        switch (value)
        {
            case "book":
                return BOOK;
            case "road":
                return ROAD;
            case "mother":
                return MOTHER;
            case "pet":
                return PET;
            case "company":
                return COMPANY;
            case "spouse":
                return SPOUSE;
            case "school":
                return SCHOOL;
        }
        return null;
    }
}
