package com.example.semestral_work.domain;

import com.example.semestral_work.domain.enums.GameStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "games")
public class GameInstance {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "a1")
    private String a1;

    @Column(name = "a2")
    private String a2;

    @Column(name = "a3")
    private String a3;

    @Column(name = "b1")
    private String b1;

    @Column(name = "b2")
    private String b2;

    @Column(name = "b3")
    private String b3;

    @Column(name = "c1")
    private String c1;

    @Column(name = "c2")
    private String c2;

    @Column(name = "c3")
    private String c3;

    @CollectionTable(name = "game_status", joinColumns = @JoinColumn(name = "game_id"))
    @Enumerated(EnumType.STRING)
    private GameStatus status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "currentPlayer")
    private User currentPlayer;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "winner")
    private User winner;

    public GameInstance(){
    }

    public void setCurrentPlayer(User currentPlayer){
        this.currentPlayer = currentPlayer;
    }

    public User getCurrentPlayer(){
        return currentPlayer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getA1() {
        return a1;
    }

    public void setA1(User player) {
        if (player != null) {
            this.a1 = player.getUsername();
        } else {
            this.a1 = null;
        }
    }

    public String getA2() {
        return a2;
    }

    public void setA2(User player) {
        if (player != null) {
            this.a2 = player.getUsername();
        } else {
            this.a2 = null;
        }
    }

    public String getA3() {
        return a3;
    }

    public void setA3(User player) {
        if (player != null) {
            this.a3 = player.getUsername();
        } else {
            this.a3 = null;
        }
    }

    public String getB1() {
        return b1;
    }

    public void setB1(User player) {
        if (player != null) {
            this.b1 = player.getUsername();
        } else {
            this.b1 = null;
        }
    }

    public String getB2() {
        return b2;
    }

    public void setB2(User player) {
        if (player != null) {
            this.b2 = player.getUsername();
        } else {
            this.b2 = null;
        }
    }

    public String getB3() {
        return b3;
    }

    public void setB3(User player) {
        if (player != null) {
            this.b3 = player.getUsername();
        } else {
            this.b3 = null;
        }
    }

    public String getC1() {
        return c1;
    }

    public void setC1(User player) {
        if (player != null) {
            this.c1 = player.getUsername();
        } else {
            this.c1 = null;
        }
    }

    public String getC2() {
        return c2;
    }

    public void setC2(User player) {
        if (player != null) {
            this.c2 = player.getUsername();
        } else {
            this.c2 = null;
        }
    }

    public String getC3() {
        return c3;
    }

    public void setC3(User player) {
        if (player != null) {
            this.c3 = player.getUsername();
        } else {
            this.c3 = null;
        }
    }

    public void setWinner(User winner){
        this.winner = winner;
    }

    public User getWinner(){
        return winner;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public String getPositionValue(String position){
        switch (position){
            case "a1":return getA1();
            case "a2":return getA2();
            case "a3":return getA3();
            case "c1":return getC1();
            case "c2":return getC2();
            case "c3":return getC3();
            case "b1":return getB1();
            case "b2":return getB2();
            case "b3":return getB3();
            default:return null;
        }
    }

    public boolean setPositionValue(String position, User player){
        boolean result = false;
        switch (position){
            case "a1": {
                setA1(player);
                result = true;
                break;
            }
            case "a2": {
                setA2(player);
                result = true;
                break;
            }
            case "a3": {
                setA3(player);
                result = true;
                break;
            }
            case "c1": {
                setC1(player);
                result = true;
                break;
            }
            case "c2": {
                setC2(player);
                result = true;
                break;
            }
            case "c3": {
                setC3(player);
                result = true;
                break;
            }
            case "b1": {
                setB1(player);
                result = true;
                break;
            }
            case "b2": {
                setB2(player);
                result = true;
                break;
            }
            case "b3": {
                setB3(player);
                result = true;
                break;
            }
        }
        return result;
    }

}
