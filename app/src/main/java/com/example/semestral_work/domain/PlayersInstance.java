package com.example.semestral_work.domain;

import javax.persistence.*;

@Entity
@Table(name = "t_players")
public class PlayersInstance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "first_user")
    private User first_user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "second_user")
    private User second_user;

    @Column(unique = true)
    @JoinColumn(name = "game_id")
    private int gameId;

    public PlayersInstance(User first_user, User second_user, int gameId) {
        this.first_user = first_user;
        this.second_user = second_user;
        this.gameId = gameId;
    }

    public PlayersInstance() {

    }

    public User getFirst_user() {
        return first_user;
    }

    public void setFirst_user(User first_user) {
        this.first_user = first_user;
    }

    public User getSecond_user() {
        return second_user;
    }

    public void setSecond_user(User second_user) {
        this.second_user = second_user;
    }

}
