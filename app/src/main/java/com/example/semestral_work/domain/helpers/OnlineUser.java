package com.example.semestral_work.domain.helpers;

public class OnlineUser {
    private Long id;
    private String username;
    private Boolean isFriend;

    public OnlineUser(Long id, String username, Boolean isFriend) {
        this.id = id;
        this.username = username;
        this.isFriend = isFriend;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public Boolean getFriend() {
        return isFriend;
    }
}
