package com.example.semestral_work.domain.helpers;

import com.example.semestral_work.domain.User;
import com.example.semestral_work.domain.enums.RelationshipStatus;

public class RelationshipUser {
    private User user;
    private RelationshipStatus status;

    public RelationshipUser(User user, RelationshipStatus status) {
        this.user = user;
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public RelationshipStatus getStatus() {
        return status;
    }
}
