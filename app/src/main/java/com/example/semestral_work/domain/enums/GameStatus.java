package com.example.semestral_work.domain.enums;

public enum GameStatus {
    WAITING,
    INPROGRESS,
    ENDED
}
