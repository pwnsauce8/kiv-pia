package com.example.semestral_work.service;

import com.example.semestral_work.config.IAuthenticationFacade;
import com.example.semestral_work.domain.Relationship;
import com.example.semestral_work.domain.User;
import com.example.semestral_work.domain.enums.RelationshipStatus;
import com.example.semestral_work.domain.enums.Role;
import com.example.semestral_work.domain.enums.UserStatus;
import com.example.semestral_work.domain.helpers.OnlineUser;
import com.example.semestral_work.domain.helpers.RelationshipUser;
import com.example.semestral_work.repos.RelationshipRepo;
import com.example.semestral_work.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    @Qualifier("sessionRegistry")
    private SessionRegistry sessionRegistry;

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RelationshipRepo relationshipRepo;

    @Autowired
    private MailSender mailSender;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private GamePlayService gamePlayService;

    long MAX_DURATION = MILLISECONDS.convert(10, MINUTES);

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepo.findByEmail(email);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return user;
    }

    public boolean deleteUser(int id){
        User user = userRepo.findById(id);
        if (user != null){
            // Check if exist in game list
            gamePlayService.deleteAllRelatedGames(user);
            // Delete all relationships
            deleteAllRelatedRelationships(user);
            
            try {
                userRepo.delete(user);
                return true;
            }
            catch (Exception ex){
                ex.printStackTrace();
                return false;
            }
        }
        else return false;
    }

    private void deleteAllRelatedRelationships (User user) {
        List<Relationship> relationships = relationshipRepo.findAll();

        for (Relationship relationship : relationships) {
            if (relationship.getFirst_user().getId().equals(user.getId()) || relationship.getSecond_user().getId().equals(user.getId())) {
                // Relationship with selected user must be deleted
                relationshipRepo.delete(relationship);
            }
        }
    }


    /**
     * Check if secret question is correct
     * @param user User instance
     * @param question question
     * @param answer answer
     * @return true/false
     */
    public boolean checkQuestion(User user, String question, String answer) {
        User userFromDb = userRepo.findByUsername(user.getUsername());

        if (userFromDb == null) {
            return false;
        }

        if (!user.getQuestion().toString().equals(question)){
            return false;
        }

        return user.getAnswer().equals(answer);
    }

    /**
     * Add user
     * @param user User instance
     * @return true/false
     */
    public boolean addUser(User user) {
        User userFromDb = userRepo.findByUsername(user.getUsername());

        if (userFromDb != null) {
            return false;
        }

        user.setQuestion(user.getQuestion());
        user.setAnswer(user.getAnswer());
        user.setActive(false);
        user.setRoles(Collections.singleton(Role.USER));
        user.setActivationCode(UUID.randomUUID().toString());
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        userRepo.save(user);

        // Send activation link
        String message = String.format(
                "Hello, %s! \n" +
                        "Welcome to Tic-tac-toe. Please, visit next link: http://localhost:8080/activate/%s",
                user.getUsername(),
                user.getActivationCode()
        );
        sendMessage(user, "Activation code", message);

        return true;
    }

    public boolean addAdmin(User user) {
        User userFromDb = userRepo.findByUsername(user.getUsername());

        if (userFromDb != null) {
            return false;
        }

        user.setRoles(Collections.singleton(Role.ADMIN));
        user.setActive(true);
        user.setEmail("admin@admin.com");
        user.setPassword(passwordEncoder.encode("admin"));
        user.setQuestion("Book");
        user.setAnswer("Book");

        userRepo.save(user);
        return true;
    }

    /**
     * Send message to user
     * @param user User instance
     * @param subject
     * @param message
     */
    public void sendMessage(User user, String subject, String message) {
        if (!StringUtils.isEmpty(user.getEmail())) {
            mailSender.send(user.getEmail(), subject, message);
        }
    }

    /**
     * Activate user
     * @param code unique code
     * @return treu/false
     */
    public boolean activateUser(String code) {
        User user = userRepo.findByActivationCode(code);

        if (user == null) {
            return false;
        }

        user.setActivationCode(null);
        user.setActive(true);
        user.setStatus(UserStatus.INLOBBY);

        userRepo.save(user);

        return true;
    }

    /**
     * Set user active
     * @param code unique code
     * @return User instance
     */
    public User getActiveUser(String code) {
        User user = userRepo.findByActivationCode(code);

        if (user == null) {
            return null;
        }

        user.setActivationCode(null);
        user.setActive(true);

        userRepo.save(user);

        return user;
    }

    /**
     * Find all users
     * @return List of users
     */
    public List<User> findAll() {
        return userRepo.findAll();
    }

    /**
     * Get online users
     * @return list of users
     */
    public List<OnlineUser> getOnlineUsers() {
        List<OnlineUser> online = new ArrayList<>();
        final List<Object> allPrincipals = sessionRegistry.getAllPrincipals();

        for(final Object principal : allPrincipals) {
            if(principal instanceof User) {
                final User user = (User) principal;

                List<SessionInformation> sessionInformations = sessionRegistry.getAllSessions(user, false);
                if (sessionInformations.size() > 0) {
                    // Delete all sessions if needed
                    for (SessionInformation info:sessionInformations) {
                        Date last = info.getLastRequest();
                        Date now = new Date();

                        long duration = now.getTime() - last.getTime();
                        if (duration >= MAX_DURATION) {
                            info.expireNow();
                        }
                    }
                    if (!sessionInformations.get(0).isExpired()) {
                        OnlineUser exist = online.stream()
                                .filter(x -> x.getId() == user.getId())
                                .findAny()
                                .orElse(null);
                        if (exist == null) {
                            Relationship relationshipFromDB = relationshipRepo.findAllByFirst_userAndSecond_user(this.getLoggedUser(), user);
                            online.add(new OnlineUser(user.getId(), user.getUsername(), relationshipFromDB != null));
                        } else {
                            sessionInformations.get(0).expireNow();
                        }
                    }
                }
            }
        }
        return online;
    }

    public boolean isOnline(User user) {
        List<OnlineUser> onlineUsers = this.getOnlineUsers();
        OnlineUser onlineUser = onlineUsers.stream()
                .filter(x -> user.getId().equals(x.getId()))
                .findAny()
                .orElse(null);
        return onlineUser != null;
    }

    /**
     * Get user by email
     * @param email
     * @return User instance
     */
    public boolean userExist(String email) {
        return userRepo.findByEmail(email) == null;
    }

    /**
     * Save user
     * @param userName User name
     * @param form
     */
    public void saveUser(String userName, Map<String, String> form) {
        User user = userRepo.findByUsername(userName);
        Set<String> roles = Arrays.stream(Role.values())
                .map(Role::name)
                .collect(Collectors.toSet());

        user.getRoles().clear();

        for (String key : form.keySet()) {
            if (roles.contains(key)) {
                user.getRoles().add(Role.valueOf(key));
            }
        }

        userRepo.save(user);
    }

    /**
     * Save user
     * @param user User instance
     */
    public void saveUser(User user) {
        userRepo.save(user);
    }

    /**
     * Is password changed
     */
    private boolean isChanged (String s1, String s2) {
        return  (s1 != null && !s1.equals(s2)) ||
                (s2 != null && !s2.equals(s1));
    }

    /**
     * Update password
     * @param user User instance
     */
    public void updatePassword(User user, String username, String password, String email) {

        if (user == null) {
            user = new User();
            user.setRoles(new HashSet<Role>() {{
                add(Role.USER);
            }});
        }

        String userEmail = user.getEmail();
        String userName = user.getUsername();

        boolean isEmailChanged = isChanged(email, userEmail);
        boolean isUsernameChanged = isChanged(username, userName);

        if (isEmailChanged) {
            user.setEmail(email);

            if (!StringUtils.isEmpty(email)) {
                user.setActive(false);
                user.setActivationCode(UUID.randomUUID().toString());
            }
        }

        if (isUsernameChanged && !StringUtils.isEmpty(username)) {
            user.setUsername(username);
        }

        if (!StringUtils.isEmpty(password)) {
            user.setPassword(passwordEncoder.encode(password));
        }

        userRepo.save(user);
    }

    /**
     * Get logged user
     * @return User instance
     */
    public User getLoggedUser() {
        Authentication authentication = authenticationFacade.getAuthentication();
        User tmp = (User) authentication.getPrincipal();
        return (User) loadUserByUsername(tmp.getEmail());
    }

    public User getUserByName(String name) {
        return userRepo.findByUsername(name);
    }

    public User getUserById(long id) {
        return userRepo.findById(id);
    }

    /**
     * Set user online status
     * @param user
     * @param online true/false
     */
    public void setOnline(User user, boolean online) {
        user.setOnline(online);
        userRepo.save(user);
    }

    /**
     * Add user to friend list
     * @param currentUser
     * @param user
     * @param status
     * @return
     */
    public Relationship subscribe(User currentUser, User user, RelationshipStatus status) {
        Relationship relationshipFromDB = relationshipRepo.findAllByFirst_userAndSecond_user(currentUser, user);
        if (relationshipFromDB != null) {
            return null;
        }

        Relationship relationship = new Relationship();

        relationship.setFirst_user(currentUser);
        relationship.setSecond_user(user);
        relationship.setStatus(status);

        relationshipRepo.save(relationship);
        return relationship;
    }

    /**
     * Update friendship status
     * @param currentUser
     * @param user
     * @param status
     * @return
     */
    public Relationship updateStatus (User currentUser, User user, RelationshipStatus status) {
        Relationship relationshipFromDB = relationshipRepo.findAllByFirst_userAndSecond_user(currentUser, user);
        if (relationshipFromDB == null) {
            return null;
        }
        relationshipFromDB.setStatus(status);
        relationshipRepo.save(relationshipFromDB);
        return relationshipFromDB;
    }

    /**
     * Delete user from friend list
     * @param currentUser
     * @param user
     */
    public void unsubscribe(User currentUser, User user) {
        Relationship relationship = relationshipRepo.findAllByFirst_userAndSecond_user(currentUser, user);
        relationshipRepo.delete(relationship);
    }

    /**
     * Get user friends
     * @param user
     * @return
     */
    public  List<RelationshipUser> getRelationships(User user) {
        List<RelationshipUser> users = new ArrayList<>();
        List<OnlineUser> onlineUsers = this.getOnlineUsers();

        for (Relationship relationship1 : relationshipRepo.findAll()) {
            if (relationship1.getFirst_user().getId().equals(user.getId())) {
                User friend = relationship1.getSecond_user();
                friend.setOnline(isOnline(friend));
                users.add(new RelationshipUser(friend, relationship1.getStatus()));
            }
        }
        return users;
    }

    /**
     * Find user by email
     * @param email
     * @return
     */
    public User getUserByEmail(String email) {
        return userRepo.findByEmail(email);
    }

    /**
     * Destroy session for user
     * @param user User instance
     */
    public void destroySession(User user) {
        for (Object tmp: sessionRegistry.getAllPrincipals()) {
            if (((User)tmp).getId().equals(user.getId())) {
                List<SessionInformation> sessionInformations = sessionRegistry.getAllSessions(tmp, false);
                for (SessionInformation session: sessionInformations) {
                    session.expireNow();
                }
            }
        }
    }
}
