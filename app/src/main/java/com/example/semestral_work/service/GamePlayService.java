package com.example.semestral_work.service;

import com.example.semestral_work.domain.GameInstance;
import com.example.semestral_work.domain.PlayersInstance;
import com.example.semestral_work.domain.User;
import com.example.semestral_work.domain.enums.GameStatus;
import com.example.semestral_work.domain.enums.UserStatus;
import com.example.semestral_work.domain.helpers.ListedGameSimpleForm;
import com.example.semestral_work.repos.GameInstanceRepo;
import com.example.semestral_work.repos.GamePlayersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GamePlayService {

    @Autowired
    private UserService userSevice;

    @Autowired
    GameInstanceRepo gameInstanceRepo;

    @Autowired
    GamePlayersRepo playersInstanceRepo;

    /**
     * Change user status (INGAME, INLOBBY)
     * @param playerName User instance
     * @param status User status
     * @return User instance
     */
    public User updateUserStatus(String playerName, UserStatus status) {
        User player = userSevice.getUserByName(playerName);
        if (player == null) {
            return null;
        }
        player.setStatus(status);

        userSevice.saveUser(player);
        return player;
    }

    /**
     * Create new game
     * @param playerName Player instance
     * @return Game instance
     */
    public GameInstance createNewGame(String playerName){
        try {
            GameInstance gameInstance = new GameInstance();
            User player = updateUserStatus(playerName, UserStatus.INGAME);

            if (player == null) return null;

            gameInstance.setStatus(GameStatus.WAITING);
            gameInstanceRepo.save(gameInstance);

            PlayersInstance playersInstance = playersInstanceRepo.findByGameId(gameInstance.getId());
            if (playersInstance == null) {
                // create new
                playersInstance = new PlayersInstance(player, null, gameInstance.getId());
                playersInstanceRepo.save(playersInstance);
            }

            return gameInstance;
        }
        catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Join existing game
     * @param id Game id
     * @param playerName User instance
     * @return Game instance
     */
    public GameInstance joinGame(int id, String playerName){
        try {
            GameInstance gameInstance = gameInstanceRepo.findById(id);

            if (gameInstance.getStatus() == GameStatus.INPROGRESS) {
                return null;
            }

            User currentPlayer = updateUserStatus(playerName, UserStatus.INGAME);
            PlayersInstance playersInstance = playersInstanceRepo.findByGameId(id);

            if (gameInstance == null || currentPlayer == null || playersInstance == null) {
                return null;
            }

            if (playersInstance.getSecond_user() == null){
                playersInstance.setSecond_user(currentPlayer);
                gameInstance.setCurrentPlayer(playersInstance.getFirst_user());
                gameInstance.setStatus(GameStatus.INPROGRESS);
                
                gameInstanceRepo.saveAndFlush(gameInstance);
                return gameInstance;
            }
            else {
                updateUserStatus(playerName, UserStatus.INLOBBY);
                return gameInstance;
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
            updateUserStatus(playerName, UserStatus.INLOBBY);
            return null;
        }
    }

    /**
     * Get current state
     * @param id Game id
     * @return Game instance
     */
    public GameInstance getCurrentState(int id){
        return gameInstanceRepo.findById(id);
    }

    /**
     * Set a move
     * @param id Game id
     * @param playerName User name
     * @param move Id of cell
     * @return true/false
     */
    public boolean setMove(int id, String playerName, String move){
        GameInstance gameInstance = gameInstanceRepo.findById(id);
        User currentPlayer = gameInstance.getCurrentPlayer();
        PlayersInstance playersInstance = playersInstanceRepo.findByGameId(id);

        if (currentPlayer == null || playersInstance == null) {
            return false;
        }

        try {
            if (currentPlayer.getUsername().equals(playerName) &&
                    playersInstance.getSecond_user() != null && playersInstance.getFirst_user() != null
                    && gameInstance.getStatus() == GameStatus.INPROGRESS){
                if (gameInstance.getPositionValue(move) == null){
                    if (gameInstance.setPositionValue(move, currentPlayer)){
                        User winner = calculateResult(gameInstance);

                        if (!playersInstance.getFirst_user().equals(currentPlayer)) {
                            gameInstance.setCurrentPlayer(playersInstance.getFirst_user());
                        }
                        if (!playersInstance.getSecond_user().equals(currentPlayer)) {
                            gameInstance.setCurrentPlayer(playersInstance.getSecond_user());
                        }

                        if (winner != null){
                            gameInstance.setStatus(GameStatus.ENDED);
                            gameInstance.setWinner(winner);
                        }
                        gameInstanceRepo.saveAndFlush(gameInstance);
                        return true;
                    }
                }
            }
            return false;
        }
        catch (Exception ex){
            return false;
        }
    }

    /**
     * Get winner
     * @param id Game id
     * @return User instance
     */
    public User getWinner(int id){
        GameInstance gameInstance = gameInstanceRepo.findById(id);
        return gameInstance.getWinner();
    }

    /**
     * Delete game
     * @param id Game id
     * @return true/false
     */
    public boolean deleteGame(int id){
        GameInstance gameInstance = gameInstanceRepo.findById(id);
        if (gameInstance!=null){
            if (gameInstance.getStatus() == GameStatus.INPROGRESS) return false;
            try {
                PlayersInstance playersInstance = playersInstanceRepo.findByGameId(id);
                if (playersInstance != null) {
                    playersInstanceRepo.delete(playersInstance);
                }
                gameInstance.setCurrentPlayer(null);
                gameInstance.setWinner(null);
                gameInstance = gameInstanceRepo.save(gameInstance);
                gameInstanceRepo.delete(gameInstance);
                return true;
            }
            catch (Exception ex){
                ex.printStackTrace();
                return false;
            }
        }
        else return false;
    }

    /**
     * End game
     * @param id Game id
     * @param player User instance
     * @return true/false
     */
    public boolean endGame(int id, String player){
        User user = updateUserStatus(player, UserStatus.INLOBBY);
        GameInstance gameInstance = gameInstanceRepo.findById(id);
        PlayersInstance playersInstance = playersInstanceRepo.findByGameId(id);

        if (playersInstance == null && gameInstance == null) {
            return false;
        }

        try {
            // If game not ended
            if (gameInstance.getWinner() == null && gameInstance.getStatus() != GameStatus.ENDED) {
                if (playersInstance.getSecond_user() == null) {
                    gameInstance.setStatus(GameStatus.ENDED);
                    gameInstanceRepo.delete(gameInstance);
                    return true;
                } else {
                    // Set winner
                    if (!playersInstance.getFirst_user().equals(user)) {
                        gameInstance.setWinner(playersInstance.getFirst_user());
                    }
                    if (!playersInstance.getSecond_user().equals(user)) {
                        gameInstance.setWinner(playersInstance.getSecond_user());
                    }
                    gameInstance.setStatus(GameStatus.ENDED);
                }
            }

            gameInstanceRepo.save(gameInstance);
            return true;
        }
        catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Calculate result
     * @param gameInstance game instance
     * @return Winner instance
     */
    private User calculateResult(GameInstance gameInstance){
        String a1,a2,a3,b1,b2,b3,c1,c2,c3;

        a1=gameInstance.getA1();
        a2=gameInstance.getA2();
        a3=gameInstance.getA3();
        b1=gameInstance.getB1();
        b2=gameInstance.getB2();
        b3=gameInstance.getB3();
        c1=gameInstance.getC1();
        c2=gameInstance.getC2();
        c3=gameInstance.getC3();

        if((a1!=null && a2!=null && a3!=null)&&(a1.equals(a2) & a1.equals(a3))){
            return userSevice.getUserByName(gameInstance.getA1());
        }
        if((b1!=null && b2!=null && b3!=null)&&(b1.equals(b2) & b1.equals(b3))){
            return userSevice.getUserByName(gameInstance.getB1());
        }
        if((c1!=null && c2!=null && c3!=null)&&(c1.equals(c2) & c1.equals(c3))){
            return userSevice.getUserByName(gameInstance.getC1());
        }
        if((a1!=null && b1!=null && c1!=null)&&(a1.equals(b1) & a1.equals(c1))){
            return userSevice.getUserByName(gameInstance.getA1());
        }
        if((a2!=null && b2!=null && c2!=null)&&(a2.equals(b2) & a2.equals(c2))){
            return userSevice.getUserByName(gameInstance.getA2());
        }
        if((a3!=null && b3!=null && c3!=null)&&(a3.equals(b3) & a3.equals(c3))){
            return userSevice.getUserByName(gameInstance.getA3());
        }
        if((a1!=null && b2!=null && c3!=null)&&(a1.equals(b2) & a1.equals(c3))){
            return userSevice.getUserByName(gameInstance.getA1());
        }
        if((c1!=null && b2!=null && a3!=null)&&(c1.equals(b2) & c1.equals(a3))){
            return userSevice.getUserByName(gameInstance.getC1());
        }
        return null;
    }

    /**
     * Reset game
     * @param id Game id
     * @return Game instance
     */
    public GameInstance resetGame(int id) {
        GameInstance gameInstance = gameInstanceRepo.findById(id);
        PlayersInstance playersInstance = playersInstanceRepo.findByGameId(id);

        if (playersInstance == null && gameInstance == null) {
            return null;
        }

        try {
            if (playersInstance.getSecond_user() == null) {
                gameInstance.setStatus(GameStatus.WAITING);
            } else {
                gameInstance.setCurrentPlayer(playersInstance.getSecond_user());
                gameInstance.setStatus(GameStatus.INPROGRESS);
            }

            gameInstance.setWinner(null);
            gameInstance.setPositionValue("a1",null);
            gameInstance.setPositionValue("a2",null);
            gameInstance.setPositionValue("a3",null);
            gameInstance.setPositionValue("b1",null);
            gameInstance.setPositionValue("b2",null);
            gameInstance.setPositionValue("b3",null);
            gameInstance.setPositionValue("c1",null);
            gameInstance.setPositionValue("c2",null);
            gameInstance.setPositionValue("c3",null);

            gameInstance = gameInstanceRepo.save(gameInstance);
            return gameInstance;
        }
        catch (Exception ex){
            ex.printStackTrace();
            return gameInstance;
        }
    }

    /**
     * Get list of the games by status
     * @param gameStatus Game status
     * @return List of the games
     */
    public List<ListedGameSimpleForm> getGameList(GameStatus gameStatus) {
        List<ListedGameSimpleForm> newGames = new ArrayList<>();

        for (GameInstance item: gameInstanceRepo.findAll()) {
            if (item.getStatus() == gameStatus){
                newGames.add(new ListedGameSimpleForm(item.getId(), playersInstanceRepo.findByGameId(item.getId()), item.getStatus(), item.getWinner()));
            }
        }
        return newGames;
    }

    public void deleteAllRelatedGames(User user) {
        List<GameInstance> gameInstances = gameInstanceRepo.findAll();

        for (GameInstance game : gameInstances) {
            PlayersInstance playersInstance = playersInstanceRepo.findByGameId(game.getId());
            if (playersInstance.getFirst_user().getId().equals(user.getId()) || playersInstance.getSecond_user().getId().equals(user.getId())) {
                // Game with selected user must be deleted
                playersInstanceRepo.delete(playersInstance);
                gameInstanceRepo.delete(game);
            }
        }
    }
}