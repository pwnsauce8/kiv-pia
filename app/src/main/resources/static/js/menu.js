$(document).ready(function() {
    $("#resetPasswordBtn").click( function(e) {
        $.ajax({
            url: "/api/user/resetPass",
            method: "POST",
            data: { email: "Test", "${_csrf.parameterName}" : "${_csrf.token}"},
        });
    });

    $("#pass").keyup(function(){
        check_pass();
        pass_equal();
    });

    $("#pass2").keyup(function(){
        pass_equal();
    });

    $(".resetGameLogs").click( function(e) {
        e.preventDefault();
        var game_id = $(this).attr('id');

        // Join the game
        joinGame(game_id);
    });
});

function check_pass()
{
    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&amp;\*])(?=.{8,})");
    var mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");

    var val = document.getElementById("pass").value;

    if(strongRegex.test(val)) {
        document.getElementById("passwordEval").innerHTML = "Perfect password!";
    } else if(mediumRegex.test(val)) {
        document.getElementById("passwordEval").innerHTML = "Strong password";
    } else {
        document.getElementById("passwordEval").innerHTML = "Weak password";
    }
}

function pass_equal()
{
    var pass1 = document.getElementById("pass").value;
    var pass2 = document.getElementById("pass2").value;
    var button = document.getElementById("saveBtn");

    if(pass1 === pass2) {
        if (button) {
            document.getElementById("passwordEval2").innerHTML = "";
            button.disabled = false;
        }
    } else {
        document.getElementById("passwordEval2").innerHTML = "Passwords are different!";
        if (button) {
            button.disabled = true;
        }
    }
}


