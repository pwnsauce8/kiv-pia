function getOnlineUsers(){
    $('#onlineUsers').load('/main #onlineUsers');
}

function getFriends(){
    $('.lobbyTable').load('/main .lobbyTable');
}

function getGameList(){
    $('#friends').load('/main #friends');
}

function approve_request(userId, userId2, userIdName) {
    if (userId !== "" && userId !== ""){
        var mess = "User " + userIdName + " added you to friend list";
        stompClientUser.send("/topic/userNotification/" + userId, {},
            JSON.stringify({content: mess, userFrom: userId2, userTo: userId, type: "APPROVE"}
            ));

        $.ajax({
            url: "/api/friends/approveRequest",
            dataType: "json",
            method: "POST",
            data: {
                user1: userId,
                user2: userId2
            },
            success: function(data) {
                refresh_data();
            }
        });
    }
}

function joinGame (gameid) {
    var name = document.getElementById("username").innerText;
    if(name!==""){
        $.ajax({
            url: "/api/tictactoe/game/" + gameid + "/connect",
            dataType: "json",
            method: "POST",
            data: {
                id: gameid,
                player: name
            },
            success: function(result) {
                window.location.href = "/game/?id=" + result.id + "&name=" + name;
            }
        });
    }
}

function createGame (name) {
    $.ajax({
        url: "/api/tictactoe/game/newgame",
        dataType: "json",
        method: "POST",
        data: { player: name },
        success: function(result) {
            window.location.href = "/game/?id=" + result.id + "&name=" + name;
        }
    });
}

