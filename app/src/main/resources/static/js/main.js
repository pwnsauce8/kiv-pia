const timeoutInt = 5000;

setInterval(function(){
    refresh_data();
}, timeoutInt)

// Connect to socket
var user = document.getElementById("userId").innerText;
if (user) {
    connectToSocketUser(user);
}

$(document).ready(function() {
    $('body').on('click', '.deleteFriend', function (e) {
        e.preventDefault();
        var userId = $(this).attr('id');
        var userId2 = document.getElementById("userId").innerText;

        if(userId !== "" && userId2 !== ""){
            $.ajax({
                url: "/api/friends/delete",
                dataType: "json",
                method: "POST",
                data: {
                    user1: userId,
                    user2: userId2
                },
                success: function() {
                    refresh_data();
                }
            });
        }
    });

    $("#newGame").click( function(e) {
        var name = document.getElementById("username").innerText;
        if(name !== "") {
            createGame(name);
        }
    });

    $('body').on('click', '.delete', function (e) {
        e.preventDefault();
        var user_name1 =  $(this).parents("tr").find(".firstPlayerName").text();
        var user_name2 =  $(this).parents("tr").find(".secondPlayerName").text();
        var current_user = document.getElementById("username").innerText;
        var message = document.getElementById("message");
        var gameid = $(this).attr('id');

        if(current_user !== ""){
            $.ajax({
                url: "/api/tictactoe/game/" + gameid + "/delete",
                dataType: "json",
                method: "POST",
                data: {
                    id: gameid,
                    player: current_user
                },
                success: function() {
                    refresh_data();
                },
                error: function () {
                    message.innerText = "You cannot delete this game."
                    message.classList.remove('hidden');
                }
            });
        }else {
            alert("Error");
        }
    });

    $('body').on('click', '.connect', function (e) {
        e.preventDefault();
        joinGame($(this).attr('id'));
    });


    $('body').on('click', '.addFriend', function (e) {
        e.preventDefault()
        var userId = $(this).attr('id');
        var userIdName = $(this).parents("tr").find("#onlineUserName").text();
        var userId2 = document.getElementById("userId").innerText;
        console.log("Add friend: " + userId + ":" + userId2 + ":" + userIdName);

        // Send message
        var mess = "User " + userIdName + " want you to add to friends";
        stompClientUser.send("/topic/userNotification/" + userId, {},
            JSON.stringify({content: mess, userFrom: userId2, userTo: userId, type: "ASK"}
            ));

        // Add to friend
        if (userId !== "" && userId !== ""){
            $.ajax({
                url: "/api/friends/add",
                dataType: "json",
                method: "POST",
                data: {
                    user1: userId,
                    user2: userId2
                },
                success: function() {
                    refresh_data();
                }
            });
        } else {
            alert("Error");
        }
    });

    $('body').on('click', '.addFriend2', function (e) {
        e.preventDefault()
        var userId = $(this).attr('id');
        var userId2 = document.getElementById("userId").innerText;

        approve_request(userId, userId2, userId2);
    });
});

function refresh_data() {
    $.get("getFriends").done(function(fragment) {
        if (fragment !== null) {
            var el = document.createElement( 'html' );
            el.innerHTML = fragment;
            var userList = el.getElementsByClassName("friendClass");
            var userList2 = el.getElementsByClassName("onlineClass");
            var gameClass = el.getElementsByClassName("gameClass");

            $('#friends').replaceWith(userList);
            $('#onlineUsers').replaceWith(userList2);
            $('#gameList').replaceWith(gameClass);
        }
    });
}