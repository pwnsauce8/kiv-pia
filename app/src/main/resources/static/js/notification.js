function showRequest(message) {
    var notificationBody = $('#notification');

    var toast =
       "<div class=\"toast ml-auto\" role=\"alert\" data-delay=\"700\" data-autohide=\"false\">\n" +
       "            <div class=\"toast-header\">\n" +
       "                <strong class=\"mr-auto text-primary\">New request!</strong>\n" +
       "                <button type=\"button\" class=\"ml-2 mb-1 close\" data-dismiss=\"toast\">&times;</button>\n" +
       "            </div>\n" +
       "            <div class=\"toast-body\"> " + message.content + " </div>\n" +
       "        </div>";

    notificationBody.append(toast);
}

function showNewFriend(message) {
    var notificationBody = $('#notification');

    var toast =
        "<div class=\"toast ml-auto\" role=\"alert\" data-delay=\"700\" data-autohide=\"false\">\n" +
        "            <div class=\"toast-header\">\n" +
        "                <strong class=\"mr-auto text-primary\">New friend!</strong>\n" +
        "                <button type=\"button\" class=\"ml-2 mb-1 close\" data-dismiss=\"toast\">&times;</button>\n" +
        "            </div>\n" +
        "            <div class=\"toast-body\"> " + message.content + " </div>\n" +
        "        </div>";

    notificationBody.append(toast);
}

function showDeleteFriend(message) {
    var notificationBody = $('#notification');

    var toast =
        "<div class=\"toast ml-auto\" role=\"alert\" data-delay=\"700\" data-autohide=\"false\">\n" +
        "            <div class=\"toast-header\">\n" +
        "                <strong class=\"mr-auto text-primary\">Friend deleted you!</strong>\n" +
        "                <button type=\"button\" class=\"ml-2 mb-1 close\" data-dismiss=\"toast\">&times;</button>\n" +
        "            </div>\n" +
        "            <div class=\"toast-body\"> " + message.content + " </div>\n" +
        "        </div>";

    notificationBody.append(toast);
}

function showAskFriend(message, request) {
    custom_confirm(message, request);
}


function custom_confirm(response) {
    $('#myModal').on('show.bs.modal', function (event) {
        var modal = $(this);
        modal.find('.modal-body').text(response.content);
    });

    $('#myModal').modal('show');

    $('#okBtn').off().on('click', function() {
        $('#myModal').modal('hide');
        processOKrequest(response);
    });

    $('#cancelBtn').off().on('click', function() {
        $('#myModal').modal('hide');
        processCancelRequest(response);
    });
}

function processOKrequest(response) {
    switch(response.type) {
        case "REQUEST":
            joinGame(response.gameId);
            break;
        case "ASK":
            // Add to friend
            approve_request(response.userFrom, response.userTo);
            break;
        case "APPROVE":
            refresh_data();
            break;
    }
}

function processCancelRequest(response) {
    switch(response.type) {
        case "REQUEST":
            var user = document.getElementById("userId").innerText;
            var mess = "User " + user + " refused your offer";
            stompClientUser.send("/topic/notification/" + response.gameId, {},
                JSON.stringify({content: mess, type: "CANCEL"}
                ));
            refresh_data();
            break;
    }
}