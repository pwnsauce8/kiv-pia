const url = 'http://localhost:8080';
let stompClient;
let stompClientUser;
let gameId;

function connectToSocket(gameId) {
    let socket = new SockJS(url + "/gameplay");
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        stompClient.subscribe("/topic/game-progress/" + gameId, function (response) {
            checkStatus();
        });

        stompClient.subscribe('/topic/chat/' + gameId, function (response) {
            onMessageReceived(response)
        });

        stompClient.subscribe('/topic/notification/' + gameId, function (response) {
            custom_confirm(JSON.parse(response.body))
        });

        stompClient.send("/topic/game-progress/", {},
            JSON.stringify({sender: username, type: 'JOIN'})
        );

        stompClient.send("/topic/game-progress/" + gameId);
    });
}

function connectToSocketUser(userId) {
    let socket = new SockJS(url + "/notification");
    stompClientUser = Stomp.over(socket);
    stompClientUser.connect({}, function (frame) {
        stompClientUser.subscribe('/topic/askFriend/' + userId, function (response) {
            custom_confirm(JSON.parse(response.body));
        });

        stompClientUser.subscribe('/topic/userNotification/' + userId, function (response) {
            custom_confirm(JSON.parse(response.body));
        });

        stompClientUser.subscribe('/topic/deleteFriend/' + userId, function () {
            refresh_data();
            console.log("Delete friend: " + userId);
        });
    });
}

function onMessageReceived(response) {
    console.log(response.body)
    var message = JSON.parse(response.body);

    var messageElement = document.createElement('li');

    if(message.type === 'JOIN') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' joined!';
    } else if (message.type === 'LEAVE') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' left!';
    } else {
        messageElement.classList.add('chat-message');

        var avatarElement = document.createElement('i');
        var avatarText = document.createTextNode(message.sender[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = getAvatarColor(message.sender);

        messageElement.appendChild(avatarElement);

        var usernameElement = document.createElement('span');
        var usernameText = document.createTextNode(message.sender);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
    }

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.content);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}