var player;
var receivedData;
var interval;
var gameOn = false;

$(document).ready(function() {
    connectToSocket(getUrlParameter("id"));
    checkStatus();
    player = getUrlParameter("name");

    if (player) {
        connectToSocketUser(player);
    }

    $(".grid").click( function() {
        var ticId = $(this).attr('id');
        makeAMove(ticId);
    });

    $("#exit").click( function() {
        $.ajax({
            url: "/api/tictactoe/game/" + getUrlParameter("id") + "/endgame",
            dataType: "json",
            method: "POST",
            data: {
                player: player
            },
            success: function() {
                checkStatus();
                window.location.href = "/main";
            }
        });
    });

    $(".askFriend").click( function(e) {
        e.preventDefault();

        var userId = $(this).attr('id');
        var userId2 = document.getElementById("userId").innerText;
        var message = "User " + userId2 + " ask you to join game";

        stompClientUser.send("/topic/askFriend/" + userId, {},
            JSON.stringify({sender: userId2, content: message, gameId: getUrlParameter("id"), type: "REQUEST"}
            ));
    });

    $("#resetGame").click( function(e) {
        e.preventDefault();

        $.ajax({
            url: "/api/tictactoe/game/" + getUrlParameter("id") + "/reset",
            dataType: "json",
            method: "POST",
            data: {},
            success: function() {
                checkStatus();
                window.location.href = "/game/?id=" + getUrlParameter("id") + "&name=" + getUrlParameter("name");
            }
        });
    });
});

function makeAMove(ticId) {
    $.ajax({
        url: "/api/tictactoe/game/" + getUrlParameter("id") + "/move",
        dataType: "json",
        method: "POST",
        data: {
            move: ticId,
            player: player
        },
        success: function () {
            checkStatus();
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function checkStatus() {
    if (getUrlParameter("id") === undefined) {
        return;
    }
    $.ajax({
        url: "/api/tictactoe/game/" + getUrlParameter("id") +"/currentstate",
        dataType: "json",
        method: "GET",
        success: function(result) {
            receivedData = result;
            changeGrid(result);
            if (receivedData.winner === null){
                setTextWhoseTurn();
            }
            if (receivedData.winner !== null){
                setGameWinner();
                clearInterval(interval);
            }
            else if (receivedData.closed === true){
                setGameIsClosed();
                clearInterval(interval);
            }
        },
        statusCode:{
            400: function () {
                setGameNotPending();
            }
        }

    });
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function setTextWhoseTurn() {
    var textPanel = $(".info-text");
    if (receivedData.currentPlayer === null){
        textPanel.text("Waiting for second user..");
    }
    else{
        textPanel.text("Turn: " + receivedData.currentPlayer.username);
    }
}

function setGameNotPending() {
    $(".info-text").text("No connection");
}

function setGameWinner() {
    $(".info-text").text("Winner: " + receivedData.winner.username + " !!!");
}

function setGameIsClosed() {
    $(".info-text").text("Game is closed");
}

function reset() {
    var grid;
    grid = {a1, a2, a3,
            b1, b2, b3,
            c1, c2, c3};

    $.each(grid, function(k, v){
        changeCellNeutral(k);
    });
}

function changeGrid(receivedResult) {
    var grid;
    grid = {a1:receivedResult.a1, a2:receivedResult.a2, a3:receivedResult.a3,
        b1:receivedResult.b1, b2:receivedResult.b2, b3:receivedResult.b3,
        c1:receivedResult.c1, c2:receivedResult.c2, c3:receivedResult.c3};

    $.each(grid,function(k, v){
        if (v!==null){
            if (v !== player){
                changeCell(k,"red");
            }
            if (v === player){
                changeCell(k,"blue");
            }
        } else {
            changeCellNeutral(k);
        }
    });
}

function changeCell(address, color) {
    var cell = $("#" + address);
    cell.removeClass("neutral");
    if (color==="blue"){
        cell.addClass("blue-x");
    }
    if (color==="red"){
        cell.addClass("red-0");
    }
}

function changeCellNeutral(address) {
    var cell = $("#" + address);
    cell.addClass("neutral");
}

