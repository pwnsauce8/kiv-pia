$(document).ready(function() {
    $(".resetPassword").click( function(e) {
        e.preventDefault();
        var userId =  $(this).attr('id');

        if(userId!==""){
            $.ajax({
                url: "/api/user/" + userId + "/reset",
                dataType: "json",
                method: "POST",
                data: {
                    id: userId
                },
                success: function() {
                    window.location.href = "/main";
                }
            });
        }
    });

    $(".deleteUser").click( function(e) {
        e.preventDefault();
        var userId = $(this).attr('id');

        if(userId !== ""){
            $.ajax({
                url: "/api/users/" + userId + "/delete",
                dataType: "json",
                method: "POST",
                data: {
                    id: userId,
                },
                success: function() {
                    window.location.href = "/user";
                }
            });
        }
    });

    $(".editUser").click( function(e) {
        var table = "";
        var user_name =  $(this).parents("tr").find(".gfgusername").text();
        var user_roles =  $(this).parents("tr").find(".gfguserrole").text().toLowerCase();
        var user_id =  $(this).parents("tr").find(".gfguserid").text();

        table += "<div>";

        table += "<input type=\"text\" readonly name=\"username\" value=\"" + user_name + "\">";
        if (user_roles.includes('admin')) {
            table += "<div class=\"form-check\">" +
                "  <input id='adminRole' type=\"checkbox\" th:value=\"ADMIN\" name=\"ADMIN\" checked>" +
                "  <label class=\"form-check-label\" for=\"adminRole\">" +
                "    Admin" +
                "  </label>" +
                "</div>";
            table += "<div class=\"form-check\">" +
                "  <input id='userRole' type=\"checkbox\" th:value=\"USER\" name=\"USER\" checked>" +
                "  <label class=\"form-check-label\" for=\"userRole\">" +
                "    User" +
                "  </label>" +
                "</div>";
        } else {
            table += "<div class=\"form-check\">" +
                "  <input id='adminRole' type=\"checkbox\" th:value=\"ADMIN\" name=\"ADMIN\">" +
                "  <label class=\"form-check-label\" for=\"adminRole\">" +
                "    Admin" +
                "  </label>" +
                "</div>";
            table += "<div class=\"form-check\">\n" +
                "  <input id='userRole' type=\"checkbox\" th:value=\"USER\" name=\"USER\" checked>" +
                "  <label class=\"form-check-label\" for=\"userRole\">" +
                "    User" +
                "  </label>" +
                "</div>";
        }

        table += "<input type=\"hidden\" value=\"" + user_id + "\" name=\"userId\">"
        table += "<input type=\"hidden\" value=\"${_csrf.token}\" name=\"_csrf\">";
        table += "<button id=\"" + user_id + "\" class=\"saveUserbtn info\" type=\"submit\">Save</button>";
        table += "</div>";

        $("#divGFG").empty();
        $("#divGFG").append(table);
    });
});

